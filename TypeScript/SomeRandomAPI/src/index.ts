import Animals from './Animals/index'
import Facts from './Facts/index'

export default {
    Animals,
    Facts
}