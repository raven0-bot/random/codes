import * as centra from "@aero/centra"

export default async function Bird() {
    const data = await centra('https://some-random-api.ml/img/bird').send()
    const { link } = data.json

    return link
}