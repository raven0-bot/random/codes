import * as centra from "@aero/centra"

export default async function redPanda() {
    const data = await centra('https://some-random-api.ml/img/red_panda').send()
    const { link } = data.json

    return link
}