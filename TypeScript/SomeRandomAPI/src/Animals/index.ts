import Dog from './Dog'
import Panda from './Panda'
import redPanda from './redPanda'
import Cat from './Cat'
import Bird from './Bird'
import Fox from './Fox'
import Koala from './Koala'

export default {
    Dog,
    Panda,
    redPanda,
    Cat,
    Bird,
    Fox,
    Koala
}