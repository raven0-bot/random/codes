import * as centra from "@aero/centra"

export default async function Panda() {
    const data = await centra('https://some-random-api.ml/img/panda').send()
    const { link } = data.json

    return link
}