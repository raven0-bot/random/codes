import * as centra from "@aero/centra"

export default async function Fox() {
    const data = await centra('https://some-random-api.ml/img/fox').send()
    const { link } = data.json

    return link
}