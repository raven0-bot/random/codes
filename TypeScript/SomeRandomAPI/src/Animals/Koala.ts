import * as centra from "@aero/centra"

export default async function Koala() {
    const data = await centra('https://some-random-api.ml/img/koala').send()
    const { link } = data.json

    return link
}