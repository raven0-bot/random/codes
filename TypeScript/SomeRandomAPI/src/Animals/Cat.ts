import * as centra from "@aero/centra"

export default async function Cat() {
    const data = await centra('https://some-random-api.ml/img/cat').send()
    const { link } = data.json

    return link
}