import * as centra from "@aero/centra"

export default async function Dog() {
    const data = await centra('https://some-random-api.ml/img/dog').send()
    const { link } = data.json

    return link
}