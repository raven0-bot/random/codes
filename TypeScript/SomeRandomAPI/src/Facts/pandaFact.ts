import * as centra from "@aero/centra"

export default async function Panda() {
    const data = await centra('https://some-random-api.ml/facts/panda').send()
    const { fact } = data.json

    return fact
}