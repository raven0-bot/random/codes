import dogFact from './dogFact'
import birdFact from './birdFact'
import catFact from './catFact'
import foxFact from './foxFact'
import koalaFact from './koalaFact'
import pandaFact from './pandaFact'

export default {
    dogFact,
    birdFact,
    catFact,
    foxFact,
    koalaFact,
    pandaFact
}