import * as centra from "@aero/centra"

export default async function Koala() {
    const data = await centra('https://some-random-api.ml/facts/koala').send()
    const { fact } = data.json

    return fact
}