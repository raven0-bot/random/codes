import * as centra from "@aero/centra"

export default async function Fox() {
    const data = await centra('https://some-random-api.ml/facts/fox').send()
    const { fact } = data.json

    return fact
}