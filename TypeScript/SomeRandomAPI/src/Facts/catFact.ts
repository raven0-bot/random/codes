import * as centra from "@aero/centra"

export default async function Cat() {
    const data = await centra('https://some-random-api.ml/facts/cat').send()
    const { fact } = data.json

    return fact
}