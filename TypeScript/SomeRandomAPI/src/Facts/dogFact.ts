import * as centra from "@aero/centra"

export default async function Dog() {
    const data = await centra('https://some-random-api.ml/facts/dog').send()
    const { fact } = data.json

    return fact
}