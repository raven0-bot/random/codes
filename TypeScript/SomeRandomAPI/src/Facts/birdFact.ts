import * as centra from "@aero/centra"

export default async function Bird() {
    const data = await centra('https://some-random-api.ml/facts/bird').send()
    const { fact } = data.json

    return fact
}